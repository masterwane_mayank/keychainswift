//
//  ViewController.swift
//  KeychainWrapper
//
//  Created by Mayank Rawat on 02/02/19.
//  Copyright © 2019 Mayank Rawat. All rights reserved.
//

import UIKit
let serverName = "WWW.mayank.com"
class ViewController: UIViewController {
    
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    private func setupUI() {
        self.nameTextField.placeholder = "Enter Name"
        self.passwordTextField.placeholder = "Enter Passowrd"
        passwordTextField.isSecureTextEntry = true
    }
    @IBAction func addBTnClicked(_ sender: Any) {
        
        var keychain: Keychain?
        keychain = Keychain(account: nameTextField?.text, password: passwordTextField?.text)
        if let status = keychain?.checkAccFromKeychain(accountName: self.nameTextField.text) {
            if(status) {
               self.passwordLabel.text = "Already in keychain"
                return
            }
        }
        do {
            try keychain?.addToKeyChain()
             self.passwordLabel.text = "account added"
            
        }
        catch KeyChainError.unhandledError(let _) {
            self.passwordLabel.text  = "unexpected error"
        } catch KeyChainError.noPassword {
           self.passwordLabel.text = "Enter password"
        } catch {
            self.passwordLabel.text  = "unexpected error"
        }
        self.nameTextField.text = ""
        self.passwordTextField.text = ""
    }
    
    @IBAction func rtrBtnClicked(_ sender: Any) {
        var keychain: Keychain?
          keychain = Keychain(account: nameTextField?.text, password: passwordTextField?.text)
        do {
            try keychain?.retRievePasswordFromKeychain(accountName: self.nameTextField.text)
             self.passwordLabel.text = "password is \((keychain?.password)!)"
        }
        catch KeyChainError.unhandledError( _) {
            self.passwordLabel.text  = "unexpected error"
        } catch KeyChainError.noPassword {
            self.passwordLabel.text = "Enter password"
        } catch {
            self.passwordLabel.text  = "unexpected error"
        }
    }
    
    @IBAction func updateBtnClicked(_ sender: Any) {
        var keychain: Keychain?
        let newPassword : String = "9410127102"
      keychain = Keychain(account: nameTextField?.text, password: passwordTextField?.text)
        if let status = keychain?.checkAccFromKeychain(accountName: self.nameTextField.text) {
            if (status) {
            do {
                try keychain?.updatePasswordValue(accountName: nameTextField?.text, password: newPassword)
                self.passwordLabel.text = "password updated"
            } catch KeyChainError.unhandledError( _) {
                self.passwordLabel.text  = "unexpected error"
            } catch KeyChainError.noPassword {
                self.passwordLabel.text = "Enter password"
            } catch {
                self.passwordLabel.text  = "unexpected error"
                }
            } else { self.passwordLabel.text = "Account not found" }
       }
    }
    @IBAction func deleteBtnClicked(_ sender: Any) {
        var keychain: Keychain?
        keychain = Keychain(account: nameTextField?.text, password: passwordTextField?.text)
        if let status = keychain?.checkAccFromKeychain(accountName: self.nameTextField.text) {
            if (status) {
                do {
                    try keychain?.deletePassworFromKeychain(accountName: nameTextField?.text)
                     self.passwordLabel.text = "password deleted"
                } catch KeyChainError.unhandledError( _) {
                    self.passwordLabel.text  = "unexpected error"
                } catch KeyChainError.noPassword {
                    self.passwordLabel.text = "Enter password"
                } catch {
                    self.passwordLabel.text  = "unexpected error"
                }
            } else {self.passwordLabel.text = "Account Not Found"}
        }
    }
}

