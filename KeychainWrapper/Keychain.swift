//
//  Keychain.swift
//  KeychainWrapper
//
//  Created by Mayank Rawat on 02/02/19.
//  Copyright © 2019 Mayank Rawat. All rights reserved.
//
enum KeyChainError: Error {
    case noPassword
    case noAccountName
    case unhandledError(status: OSStatus)
    case unexpectedError
}
import Foundation
import UIKit
class Keychain {
    var account: String?
    var password: String?
    init(account: String?, password: String?) {
        self.account = account
        self.password = password
    }
    func addToKeyChain() throws {
        guard let password = self.password , !password.isEmpty else {
           throw KeyChainError.noPassword
        }
        let account = self.account
        let pass = self.password?.data(using: .utf8)
        let query: [String: Any] = [kSecClass as String: kSecClassInternetPassword,
                                    kSecAttrServer as String: serverName, kSecAttrAccount as String: account!, kSecValueData as String: pass!]
        let status = SecItemAdd(query as CFDictionary, nil)
        guard status == errSecSuccess else {
            throw KeyChainError.unhandledError(status: status)
            
        }
        print("success in adding")
    }
    
    func retRievePasswordFromKeychain(accountName: String?) throws {
        guard let acc = accountName, !acc.isEmpty else{ throw KeyChainError.noAccountName}
        var items: CFTypeRef?
        let query: [String: Any] = [kSecClass as String: kSecClassInternetPassword, kSecAttrServer as String: serverName, kSecReturnAttributes as String: true, kSecMatchLimit as String: kSecMatchLimitOne, kSecAttrAccount as String: accountName!, kSecReturnData as String: true]
        
        let status = SecItemCopyMatching(query as CFDictionary, &items)
        
        guard status != errSecItemNotFound else { throw KeyChainError.unhandledError(status: status) }
        guard status == errSecSuccess else { throw KeyChainError.unexpectedError}
        guard let item = items as? [String: Any] ,
        let passwordData = item[kSecValueData as String] as? Data,
        let password = String(bytes: passwordData, encoding: .utf8),
            let _ = item[kSecAttrAccount as String] as? String,
            let _ = item[kSecAttrServer as String] as? String else {
                throw KeyChainError.unexpectedError
        }
        self.password = password
        print(password)
    }
    func checkAccFromKeychain(accountName: String?) -> Bool {
        guard let acc = accountName, !acc.isEmpty else {
         return false
        }
        var items: CFTypeRef?
        let query: [String: Any] = [kSecClass as String: kSecClassInternetPassword, kSecAttrServer as String: serverName, kSecMatchLimit as String: kSecMatchLimitOne, kSecReturnAttributes as String: true, kSecAttrAccount as String: accountName!, kSecReturnData as String: true]
        let status = SecItemCopyMatching(query as CFDictionary, &items)
        guard status != errSecItemNotFound else {
            return false
        }
        guard status == errSecSuccess else {
            return false
        }
        guard let item = items as? [String: Any] ,
            let passwordData = item[kSecValueData as String] as? Data,
           let _ = String(bytes: passwordData, encoding: .utf8),
            let _ = item[kSecAttrAccount as String] as? String,
            let _ = item[kSecAttrServer as String] as? String else {
               return false
        }
        print("account found")
       return true
    }
    func updatePasswordValue(accountName: String?, password: String?) throws {
        guard let acc = accountName, !acc.isEmpty  else{   throw KeyChainError.noAccountName }
        let query: [String: Any] = [kSecClass as String: kSecClassInternetPassword,
                                    kSecAttrServer as String: serverName, kSecAttrAccount as String: acc]
         let pass = password?.data(using: .utf8)
        let attributes: [String: Any] = [kSecAttrAccount as String: acc,
                                         kSecValueData as String: pass!]
        let status = SecItemUpdate(query as CFDictionary, attributes as CFDictionary)
        guard status != errSecItemNotFound else{throw KeyChainError.unhandledError(status: status) }
        guard status == errSecSuccess else {throw KeyChainError.unexpectedError}
        self.password = password
        print("password updated")
    }
    func deletePassworFromKeychain(accountName: String?) throws {
        guard let acc = accountName, !acc.isEmpty else {throw KeyChainError.noAccountName}
        let query: [String: Any] = [kSecClass as String: kSecClassInternetPassword, kSecAttrAccount as String: acc, kSecAttrServer as String: serverName]
        let status = SecItemDelete(query as CFDictionary)
        guard status != errSecItemNotFound else { throw KeyChainError.unhandledError(status: status)}
        guard status == errSecSuccess else {throw KeyChainError.unexpectedError}
        print("password Deleted")
    }
}


